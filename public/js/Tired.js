import IState from './IState.js';

class Tired extends IState{
    
    feed(pet){
        const message = "No tengo hambre, estoy muy cansado";
        return message;
    }

    play(pet){
        const message = "Ya no quiero jugar, estoy muy cansado";
        return message;
    }

    sleep(pet){
        const message = "Buenas noches... Zzz...";
        pet.setState(pet.states.Sleeping);
        setTimeout(() => {
            pet.setState(pet.states.Hungry);
        },15000);
        return message;
    }

    iAm(pet){
        const message = "Estoy cansado";
        return message;
    }
}

export default Tired;

