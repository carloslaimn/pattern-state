import IState from './IState.js';

class Sleeping extends IState {

    feed(pet) {
        const message = "Zzz...";
        return message;
    }

    play(pet) {
        const message = "Zzz...";
        return message;
    }

    sleep(pet) {
        const message = "Zzz...";
        return message;
    }

    iAm(pet) {
        const message = "Zzz...";
        return message;
    }
}

export default Sleeping;
