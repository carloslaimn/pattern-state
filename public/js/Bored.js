import IState from './IState.js';

class Bored extends IState{
    
    feed(pet){
        const message = "No tengo hambre";
        return message;
    }

    play(pet){
        const message = "¡Si, Vamos a jugar!";
        pet.setState(pet.states.Playing);
        setTimeout(() => {
            pet.setState(pet.states.Tired);
        },10000);
        return message;
    }

    sleep(pet){
        const message = "No quiero dormir, estoy aburrido";
        return message;
    }

    iAm(pet){
        const message = "Estoy aburrido, quiero jugar...";
        return message;
    }

}

export default Bored;

