import IState from './IState.js';

class Eating extends IState {

    feed(pet) {
        const message = "Ya estoy comiendo";
        return message;
    }

    play(pet) {
        const message = "Quiero seguir comiendo";
        return message;
    }

    sleep(pet) {
        const message = "Quiero seguir comiendo";
        return message;
    }

    iAm(pet) {
        const message = "Estoy comiendo";
        return message;
    }
}

export default Eating;
