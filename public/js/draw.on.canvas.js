var canvas = document.getElementById("gameCanvas");
var ctx = canvas.getContext("2d");

function drawBackground() {
    var backgroundImage = new Image();
    backgroundImage.src = "images/background-bedroom.png"; 
    backgroundImage.onload = function() {
        canvas.width = 1000;
        canvas.height = backgroundImage.height * (1000 / backgroundImage.width);
        ctx.drawImage(backgroundImage, 50, 0, canvas.width, canvas.height);
        drawsPet();
    };
}

function drawsPet() {
    var img = new Image();
    img.src = "images/pana-miguel.png";
    //img.src = "images/" + imageName;
    let x = 350;
    let y = 150;
    let newWidth = 500;
    let newHeight = 500;
    img.onload = function() {
        ctx.drawImage(img, x, y, newWidth, newHeight);
    };
}

function drawAlert(message) {
    const x = 400;  
    const y = 180;  
    const width = 200;
    const height = 100;

    const imageData = ctx.getImageData(x - ctx.lineWidth, y - ctx.lineWidth, width + ctx.lineWidth * 2, height + ctx.lineWidth * 2);

    ctx.fillStyle = 'white';
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 2;
    ctx.fillRect(x, y, width, height); 
    ctx.strokeRect(x, y, width, height); 

    ctx.fillStyle = 'black';
    ctx.font = '16px Arial';
    ctx.textAlign = 'center';
    ctx.fillText(message, x + width / 2, y + height / 2 + 5); 

    setTimeout(() => {
        ctx.putImageData(imageData, x - ctx.lineWidth, y - ctx.lineWidth);
    }, 1000);
    
}

drawBackground();

export { drawAlert, drawsPet };
