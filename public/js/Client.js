import Pet from './Pet.js';
import { drawAlert, drawsPet } from './draw.on.canvas.js'; 

const panaMiguel = new Pet();
window.panaMiguel = panaMiguel;

function ask() {
    let state = panaMiguel.iAm();
    console.log(state);
    drawAlert(state); 
}
window.ask = ask;

function feed(){
    let state = panaMiguel.feed();
    console.log(state);
    drawAlert(state); 
}
window.feed = feed;

function play(){
    let state = panaMiguel.play();
    console.log(state);
    drawAlert(state); 
}
window.play = play;

function sleep(){
    let state = panaMiguel.sleep();
    console.log(state);
    drawAlert(state); 
}
window.sleep = sleep;

export default { ask, feed, play, sleep};
