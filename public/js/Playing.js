import IState from './IState.js';

class Playing extends IState {

    feed(pet) {
        const message = "No molestes estoy jugando";
        return message;
    }

    play(pet) {
        const message = "Ya estamos jugando";
        return message;
    }

    sleep(pet) {
        const message = "Todavía no quiero dormir";
        return message;
    }

    iAm(pet) {
        const message = "Estoy jugando...";
        return message;
    }
}

export default Playing;
