class IState{

    feed(pet) {
        throw new Error('this method must be implemented by a subclass');
    }

    play(pet) {
        throw new Error('this method must be implemented by a subclass');
    }

    sleep(pet) {
        throw new Error('this method must be implemented by a subclass');
    }

    iAm(pet){
        throw new Error('this method must be implemented by a subclass');
    }
}

export default IState;