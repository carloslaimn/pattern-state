import IState from './IState.js';

class Hungry extends IState {

    feed(pet) {
        const message = "Mmm croquetas...";
        pet.setState(pet.states.Eating);
        setTimeout(() => {
            pet.setState(pet.states.Bored);
        },8000);
        return message;
    }

    play(pet) {
        const message = "No quiero jugar. Tengo hambre.";
        return message;
    }

    sleep(pet) {
        const message = "No quiero dormir. Tengo hambre.";
        return message;
    }

    iAm(pet) {
        const message = "Tengo hambre. ¿Me das algo de comer?";
        return message;
    }
}

export default Hungry;
