import Bored from './Bored.js';
import Eating from './Eating.js';
import Hungry from './Hungry.js';
import Playing from './Playing.js';
import Sleeping from './Sleeping.js';
import Tired from './Tired.js';

class Pet{
    
    constructor(){
        this.states = {
            Bored: new Bored(),
            Eating: new Eating(),
            Hungry: new Hungry(),
            Playing: new Playing(),
            Sleeping: new Sleeping(),
            Tired: new Tired()
        }
        this.currentState = this.states.Bored;
    }

    setState(newState){
        this.currentState = newState;
    }

    feed() {
        return this.currentState.feed(this);
    }

    play() {
        return this.currentState.play(this);
    }

    sleep() {
        return this.currentState.sleep(this);
    }

    iAm(){
        return this.currentState.iAm(this);
    }

}

export default Pet;