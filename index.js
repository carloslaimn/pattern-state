const express = require('express');
const app = express();
const path = require('path');

// Define la carpeta de archivos estáticos
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname,'game.html'));
  });

app.listen(3000,() => {
    console.log('NodeJS App listening on: http://localhost:3000');
})